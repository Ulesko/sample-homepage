.. post:: Nov 26, 2017
   :tags: tag1, tag2
   :author: Ulesko

Конспект с лекции "Docs like code"
==================================

* В документ стоит указывать заголовок
* Посмотреть свой проект можно по ссылке: <http://ulesko.gitlab.io/sample-homepage/>`_
* Ссылка на документацию по синтаксису: <http://www.sphinx-doc.org/en/stable/rest.html>`_
* https://www.google.ru/ - пример ссылки
* В настройках можно указать имя сайта: ulesko.gitlab.io


Стоит найти плагин для подсветки синтаксиса rst для любой программной среды

Пример вставки картинки

.. image:: Lenna.png